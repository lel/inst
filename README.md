# inst

Symlink files into the path. Defaults to `$HOME/.local/bin`. Also removes files from the inst dir with confirmation.

## Installation

```bash
git clone https://git.envs.net/lel/inst
cd inst
ln -s $(pwd)/inst ~/.local/bin
```

(or, for that last line, `./inst inst` !!! that's the whole point of this!!!)

## Usage

To install things:

```bash
inst [options] path [name]
```

When using it to install (without `-r`) `[name]` is an optional argument that symlinks it in with a different name than the filename in the original directory.

e.g., `inst inst bla` will `install inst` with the name `bla`, while `inst inst` will `install inst` with the name `inst`, because that was its original name.

It's also worth noting that what you're installing doesn't need to be in the working directory, it uses `realpath` to make it work from anywhere, so you could also do something like `inst /home/lel/Downloads/b/inst/inst` and that would work.

To uninstall things:

```bash
inst -r path
```

This will prompt you for confirmation then remove that filename from the directory where `inst` puts things (once again `~/.local/bin` by default; can be changed by env variable `$INST_DIR`, or editing line 6).

`-h` will show some help.

## Misc. notes

If something isn't working, make sure `~/.local/bin` is in your `$PATH`.

This is really bad and I know that but I don't care. I figured maybe someone would find it useful so here it is. I haven't tested this outside my own setup at all and it's entirely possible that there will be little problems that will require you to edit it slightly. idk.
